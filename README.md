# Useful Links

readme.mdからのアクセス簡略化用

##レポジトリ内リンク

[この課題内のタスクを見る](../../issues)

[MLAリストを見る](./MLA.md)

[課題レポジトリの編集履歴を見る](../../commits/all)

##レポジトリ外リンク

[学校用Google Driveフォルダ](https://drive.google.com/drive/folders/0Bxhbf9hgPMwDZHE2dmhGVzVMcTA)(新規タブで開かないため、CTRLキー押しながらアクセス)

[Schoology](https://app.schoology.com/)(新規タブで開かないため、CTRLキー押しながらアクセス)

[Managebac](https://tamagawa.managebac.com/student/)(新規タブで開かないため、CTRLキー押しながらアクセス)

[新規課題登録](https://bitbucket.org/mb21mlschool/add-new-tasks)(新規タブで開かないため、CTRLキー押しながらアクセス)

# Info

課題概要

_



# Google Drive Links
Google Driveのフォルダなどへのリンク

[Google Drive Project Folder]()(新規タブで開かないため、CTRLキー押しながらアクセス)

[Google Drive Shared Folder]()(新規タブで開かないため、CTRLキー押しながらアクセス)

[Google Drive Subject Folder]()(新規タブで開かないため、CTRLキー押しながらアクセス)